export const ingredientsMock = [
  { id: 1, title: 'Fish', group: 'fish', createdAt: '2021-08-19T13:32:00.953Z', proteins: '100', fats: '150', carbohydrates: '120', calories: '300'},
  { id: 2, title: 'Chips', group: 'fast-food',createdAt: '2021-01-19T13:32:00.953Z', proteins: '110', fats: '160', carbohydrates: '130', calories: '320' },
  { id: 3, title: 'Milk', group: 'dairy-products',createdAt: '2021-03-19T13:32:00.953Z', proteins: '120', fats: '170', carbohydrates: '140', calories: '350' },
  { id: 4, title: 'Cheese 1', group: 'dairy-products',createdAt: '2021-04-19T13:32:00.953Z', proteins: '130', fats: '180', carbohydrates: '150', calories: '360' },
  { id: 5, title: 'Cheese 2', group: 'dairy-products',createdAt: '2021-08-05T13:32:00.953Z', proteins: '140', fats: '190', carbohydrates: '160', calories: '380' },
  { id: 6, title: 'Apple', group: 'fruits',createdAt: '2021-08-05T13:32:00.953Z', proteins: '40', fats: '190', carbohydrates: '190', calories: '380' },
  { id: 7, title: 'Onion', group: 'vegetables',createdAt: '2021-08-05T13:32:00.953Z', proteins: '60', fats: '190', carbohydrates: '11', calories: '380' },
];

export const groups = [
  {value: 'fast-food', title: 'fast-food'},
  {value: 'dairy-products', title: 'dairy-products'},
  {value: 'fish', title: 'fish'},
  {value: 'fruits', title: 'fruits'},
  {value: 'vegetables', title: 'vegetables'}
];
