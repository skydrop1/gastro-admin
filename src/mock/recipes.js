import {ingredientsMock} from "./ingredients";

export const recipesMock = [
  {
    id: 1,
    title: 'Fish and chips',
    ingredients: [
      ingredientsMock[0], ingredientsMock[1]
    ],
    actions: [
      {
        title: 'Порезать',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'Пожарить',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'На пару',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      }
    ]
  },
  {
    id: 2,
    title: 'Fish and chips 1',
    ingredients: [
      ingredientsMock[0], ingredientsMock[1]
    ],
    actions: [
      {
        title: 'Порезать',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'Пожарить',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'На пару',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      }
    ]
  },
  {
    id: 3,
    title: 'Fish and chips 3',
    ingredients: [
      ingredientsMock[0], ingredientsMock[1]
    ],
    actions: [
      {
        title: 'Порезать',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'Пожарить',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      },
      {
        title: 'На пару',
        ingredients: [
          ingredientsMock[0], ingredientsMock[1]
        ],
      }
    ]
  }
];