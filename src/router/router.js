import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';
import { routes, loginRoute } from './routes';
import { LoginPage } from '../pages/login/login.page';
import { renderRoute } from './router.helpers';
import { Menu } from '../components/complex/menu/menu.component';

export const Router = () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Menu>
            {routes.map(renderRoute)}
          </Menu>
          <Route path={loginRoute}>
            <LoginPage/>
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};
