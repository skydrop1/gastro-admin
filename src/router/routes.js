import React from 'react';
import { DashboardPage } from '../pages/dashboard/dashboard.page';
import { IngredientsPage } from '../pages/ingredients/ingredients.page';
import { IngredientsAddPage } from '../pages/ingredient-add/ingredients-add.page';
import { RecipesPage } from '../pages/recipes/recipes.page';
import {IngredientsEditPage} from '../pages/ingredient-edit/ingredients-edit.page';
import { RecipesAddPage } from '../pages/recipe-add/recipes-add.page';
export const loginRoute = '/';
export const dashboardRoute = '/dashboard';
export const ingredientsRoute = '/ingredients';
export const ingredientsAddRoute = '/ingredients-add';
export const ingredientEditRoute = '/ingredients-edit/:id';
export const recipesRoute = '/recipes';
export const recipesAddRoute = '/recipes-add';

export const routes = [
  [dashboardRoute, <DashboardPage/>],
  [ingredientsRoute, <IngredientsPage/>],
  [ingredientsAddRoute, <IngredientsAddPage/>],
  [ingredientEditRoute, <IngredientsEditPage/>],
  [recipesRoute, <RecipesPage/>],
  [recipesAddRoute, <RecipesAddPage/>],
];
