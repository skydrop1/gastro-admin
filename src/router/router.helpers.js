import React from 'react';

import { Route } from 'react-router-dom';

export const renderRoute = ([path, component]) => (
  <Route path={path} key={path}>
      {component}
  </Route>
);