export const getDrawerClass = (open, defaultClass, closeClass) => {
  let className = defaultClass;

  if (!open) {
    className += ` ${closeClass}`
  }

  return className;
};