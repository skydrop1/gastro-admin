import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import BarChartIcon from '@material-ui/icons/BarChart';
import {dashboardRoute, ingredientsRoute, recipesRoute} from '../../../../router/routes';
import {Link} from '../../../../styles/common.styles';

const list = [
  {title: 'Dashboard', link: dashboardRoute, Icon: DashboardIcon},
  {title: 'Ingredients', link: ingredientsRoute, Icon: ShoppingCartIcon},
  {title: 'Recipes', link: recipesRoute, Icon: ShoppingCartIcon},
];

export const mainListItems = (
  <div>
    {list.map(({ title, link, Icon }) => (
      <ListItem button key={`menu-item-${link}`}>
        <Link to={link}>
          <ListItemIcon>
            <Icon />
          </ListItemIcon>
          <ListItemText primary={title}/>
        </Link>
      </ListItem>
    ))}

    <ListItem button>
      <ListItemIcon>
        <BarChartIcon/>
      </ListItemIcon>
      <ListItemText primary="Reports"/>
    </ListItem>
  </div>
);
