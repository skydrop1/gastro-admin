import React, { useState } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Drawer from '@material-ui/core/Drawer';
import { mainListItems } from './components/menu.items';
import { Copyright } from '../../simple/copyright/copyright.component';
import { useStyles } from './menu.styles';
import { MenuContext } from '../../../contexts/menu.context';

import clsx from 'clsx';

const MenuComponent = (props) => {
  const {
    children,
  } = props;

  const [open, setOpen] = useState(true);
  const [title, setTitle] = useState('Dashboard');
  const classes = useStyles();


  const drawerClass = clsx(classes.drawerPaper, !open && classes.drawerPaperClose);
  const appBarClass = clsx(classes.appBar, open && classes.appBarShift);
  const iconButtonClass = clsx(classes.menuButton, open && classes.menuButtonHidden);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={appBarClass}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            className={iconButtonClass}
            onClick={handleDrawerOpen}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap>
            {title}
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        open={open}
        classes={{
          paper: drawerClass
        }}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <MenuContext.Provider value={{ setTitle }}>
            {children}
          </MenuContext.Provider>
          <Box pt={4} className={classes.footer}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
};

MenuComponent.propTypes = {};

export const Menu = MenuComponent;