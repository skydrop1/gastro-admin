import React from 'react';
import PropTypes from 'prop-types';
import { DataGrid } from '@material-ui/data-grid';

const TableComponent = (props) => {
  const {cells, rows, setSelectedRows} = props;

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={cells}
        pageSize={5}
        rowsPerPageOptions={[5]}
        checkboxSelection
        onSelectionModelChange={(event) => {
          const selectedIDs = new Set(event);
          const selectedRows = rows.filter((item) => selectedIDs.has(item.id));
          setSelectedRows(selectedRows);
        }}
      />
    </div>
  );
};

TableComponent.propTypes = {
  cells: PropTypes.arrayOf(PropTypes.object).isRequired,
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export const LinkTable = TableComponent;
