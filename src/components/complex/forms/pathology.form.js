import React from 'react';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const PathologyFormComponent = (props) => {
    const {
        onSubmit,
    } = props;

    const formik = useFormik({
        initialValue: {
            title: '',
        },
        onSubmit,
    });

    const { handleSubmit, handleChange } = formik;

    return (
        <form onSubmit={handleSubmit}>
            <TextField label="Title" name="title" onChange={handleChange}/>
            <br />
            <TextField label="ваши патологии" name="title" onChange={handleChange}/>
            <br />
            <TextField label="ваши патологии" name="title" onChange={handleChange}/>
            <br />
            <TextField label="ваши патологии" name="title" onChange={handleChange}/>
            <br />
            <Button variant="contained" color="primary" type="submit">
                Submit
            </Button>
        </form>
    );
};

PathologyFormComponent.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export const PathologyForm = PathologyFormComponent;
