import React, {useContext, useEffect} from 'react';
import PropTypes from 'prop-types';
import {useFormik} from 'formik';
import * as Yup from 'yup';

import Autocomplete from '@material-ui/lab/Autocomplete';

import {MenuContext} from '../../../../contexts/menu.context';

import {TextField, Button} from './recipe.form.styles';
import { RecipeActions as Actions } from './recipe.action.component';

const getValidation = () => Yup.object().shape({
  title: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
});

let id = -1;
const RecipeFormComponent = ({onSubmit, ingredients, initialValues}) => {
  const {setTitle} = useContext(MenuContext);

  useEffect(() => {
    setTitle('Create ingredient')
  }, [setTitle]);

  const { handleSubmit, handleChange, values, errors, setFieldValue } = useFormik({
    initialValues,
    onSubmit,
    validationSchema: getValidation(),
  });

  const addAction = () => setFieldValue('actions', [{ id: id++, title: '', image: null, ingredients: [] }, ...values.actions]);

  console.log('errors: ', errors);
  return (
    <form onSubmit={handleSubmit}>
      <TextField error={Boolean(errors.title)} helpText={errors.title} label="title" name="title" value={values.title} onChange={handleChange}/>
      <Autocomplete
        multiple
        id="tags-standard"
        name="ingredients"
        options={ingredients}
        value={values.ingredients}
        onChange={(event, items) => setFieldValue('ingredients', items)}
        getOptionLabel={(option) => option.title}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="standard"
            label="Multiple values"
            placeholder="Favorites"
          />
        )}
      />
      <Actions onChange={handleChange} onSetValue={setFieldValue} items={values.actions} includes={values.ingredients} />
      <Button variant="contained" color="primary" type="button" onClick={addAction}>
        Add action
      </Button>
      <Button variant="contained" color="primary" type="submit">
        Submit
      </Button>
    </form>
  );
};

RecipeFormComponent.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
  ingredients: PropTypes.arrayOf(PropTypes.object).isRequired,
};

RecipeFormComponent.defaultProps = {
  initialValues: {
    title: '',
    ingredients: [],
    actions: []
  },
};

export const RecipeForm = RecipeFormComponent;
