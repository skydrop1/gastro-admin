import styled from 'styled-components';

import ButtonMUI from '@material-ui/core/Button';
import { TextField as OurTextField } from '../../../simple/text-field/text-field.component';

export const TextField = styled(OurTextField)`
  margin-top: 20px;
`;

export const Button = styled(ButtonMUI)`
  margin-top: 20px;
`;

export const Action = styled.div`
  width: 30%;
`;