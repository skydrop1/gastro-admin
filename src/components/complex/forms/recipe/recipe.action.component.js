import React from 'react';
import PropTypes from 'prop-types';

import { Select } from '../../../simple/select/select.component';
import { TextField, Action } from './recipe.form.styles';

const RecipeActionsComponent = ({onChange, onSetValue, items, includes}) => (
  <Action>
    {items.map((item, index) => (
      <div key={`action-${item.id}`}>
        <TextField
          label="title"
          name={`actions.${index}.title`}
          type="text"
          variant="filled"
          value={item.title}
          onChange={onChange}
        />
        <Select
          multiple
          label="includes"
          name={`actions.${index}.ingredients`}
          labelId="title"
          list={includes}
          title="includes"
          value={item.ingredients}
          onChange={(event) => {
            const {
              target: { value },
            } = event;
            onSetValue(
              `actions.${index}.ingredients`,
              typeof value === 'string' ? value.split(',') : [...item.ingredients, value],
            );
          }}
        />
      </div>
    ))}
  </Action>
);

RecipeActionsComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSetValue: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  includes: PropTypes.arrayOf(PropTypes.object).isRequired,
};

RecipeActionsComponent.defaultProps = {
  items: [],
  includes: [],
};

export const RecipeActions = RecipeActionsComponent;
