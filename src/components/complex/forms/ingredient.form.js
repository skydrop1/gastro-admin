import React, {useContext, useEffect} from 'react';
import PropTypes from 'prop-types';
import {useFormik} from 'formik';
import {MenuContext} from '../../../contexts/menu.context';
import {groups} from '../../../mock/ingredients';
import {Groups, TextField, Button} from './ingredient.form.styles';

const IngredientFormComponent = ({onSubmit}) => {
  const {setTitle} = useContext(MenuContext);

  useEffect(() => {
    setTitle('Create ingredient')
  }, [setTitle]);

  const formik = useFormik({
    initialValues: {
      title: '',
      group: '',
      createdAt: new Date,
      proteins: 0,
      fats: 0,
      carbohydrates: 0,
      calories: 0
    },
    onSubmit,
  });

  const {handleSubmit, handleChange} = formik;

  return (
    <form onSubmit={handleSubmit}>
      <TextField label="title" name="title" value={formik.values.title} onChange={handleChange}/>
      <Groups label="group" list={groups} title="groups" value={formik.values.group} onChange={handleChange}/>
      <div>
        <TextField label="proteins" name="proteins" type="number" variant="filled" value={formik.values.proteins} onChange={handleChange}/>
        <TextField label="fats" name="fats" type="number" variant="filled" value={formik.values.fats} onChange={handleChange}/>
        <TextField label="carbohydrates" name="carbohydrates" type="number" variant="filled" value={formik.values.carbohydrates} onChange={handleChange}/>
        <TextField label="calories" name="calories" type="number" variant="filled" value={formik.values.calories} onChange={handleChange}/>
      </div>
      <Button variant="contained" color="primary" type="submit">
        Submit
      </Button>
    </form>
  );
};

IngredientFormComponent.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export const IngredientForm = IngredientFormComponent;
