import React, {useContext, useEffect} from 'react';
import {useFormik} from 'formik';
import {MenuContext} from '../../../contexts/menu.context';
import {groups} from '../../../mock/ingredients';
import {Groups, TextField, Button} from './ingredient.form.styles';

const IngredientEditFormComponent = ({mutate, value, id}) => {
  const {setTitle} = useContext(MenuContext);

  useEffect(() => {
    setTitle('Create ingredient')
  }, [setTitle]);

  const formik = useFormik({
    initialValues: {
      id: id,
      title: value.title,
      group: value.group,
      createdAt: value.createdAt,
      proteins: value.proteins,
      fats: value.fats,
      carbohydrates: value.carbohydrates,
      calories: value.calories
    },
    onSubmit: (values) => {
      mutate(values)
    }
  });

  const {handleSubmit, handleChange} = formik;

  return (
    <form onSubmit={handleSubmit}>
      <TextField label="title" name="title" value={formik.values.title} onChange={handleChange}/>
      <Groups label="group" list={groups} title="groups" value={formik.values.group} onChange={handleChange}/>
      <p>
        <TextField label="proteins" name="proteins" type="number" variant="filled" value={formik.values.proteins} onChange={handleChange}/>
        <TextField label="fats" name="fats" type="number" variant="filled" value={formik.values.fats} onChange={handleChange}/>
        <TextField label="carbohydrates" name="carbohydrates" type="number" variant="filled" value={formik.values.carbohydrates} onChange={handleChange}/>
        <TextField label="calories" name="calories" type="number" variant="filled" value={formik.values.calories} onChange={handleChange}/>
      </p>
      <Button variant="contained" color="primary" type="submit">
        Submit
      </Button>
    </form>
  );
};

export const IngredientEditForm = IngredientEditFormComponent;
