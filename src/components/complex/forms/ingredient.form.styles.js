import styled from 'styled-components';
import {Select} from '../../simple/select/select.component';
import TextFieldMUI from '@material-ui/core/TextField';
import ButtonMUI from '@material-ui/core/Button';

export const Groups = styled(Select)`
  max-width: 166px;
  margin-top: 20px;
`;

export const TextField = styled(TextFieldMUI)`
  &:first-child {
    margin-left: 0;
  }

  margin-left: 20px;
  margin-top: 20px;
`;

export const Button = styled(ButtonMUI)`
  margin-top: 20px;
`;