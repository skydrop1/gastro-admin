import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash/fp';
import Button from '@material-ui/core/Button';
import { Link } from '../../../styles/common.styles';

const omitTo = _.omit('to');

const LinkButtonComponent = (props) => {
  const {
    to,
    label,
  } = props;

  const buttonProps = omitTo(props);

  return (
   <Link to={to}>
     <Button {...buttonProps}>
       {label}
     </Button>
   </Link>
  );
};

LinkButtonComponent.propTypes = {
  to: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  variant: PropTypes.string,
  color: PropTypes.string,
};

LinkButtonComponent.defaultProps = {
  variant: 'contained',
  color: 'primary',
};

export const LinkButton = LinkButtonComponent;
