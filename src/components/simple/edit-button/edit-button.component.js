import React from 'react';
import {LinkButton} from '../link-button/link-button.component';
import {ingredientEditRoute} from '../../../router/routes';

export const ButtonEdit = ({id}) => {
  return (
    <LinkButton
      to={ingredientEditRoute.replace(':id', id)}
      label="Edit"
    />
  );
};