import React from 'react';
import MuiTextField from '@material-ui/core/TextField';
import { Container } from './text-field.styles';

const TextFieldComponent = ({ helpText, ...rest}) => {
  return (
    <Container>
      <MuiTextField {...rest} />
      <span>{helpText ? helpText : null}</span>
    </Container>
  );
};

export const TextField = TextFieldComponent;
