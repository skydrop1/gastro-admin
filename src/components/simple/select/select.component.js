import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';
import MuiSelect from '@material-ui/core/Select';

const SelectComponent = ({className, list, value, title, onChange, ...rest}) => {
  return (
    <Box className={className}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{title}</InputLabel>
        <MuiSelect
          {...rest}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          name="group"
          value={value}
          onChange={onChange}
        >
          {list.map(item => {
            return <MenuItem key={`${item.title}-${item.value}`} value={item.value}>{item.title}</MenuItem>
          })}
        </MuiSelect>
      </FormControl>
    </Box>
  );
};

export const Select = SelectComponent;