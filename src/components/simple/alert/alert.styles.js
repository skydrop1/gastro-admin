import styled from 'styled-components';
import AlertMUI from '@material-ui/lab/Alert';

export const Alert = styled(AlertMUI)`
  position: fixed;
  bottom: 0;
`;