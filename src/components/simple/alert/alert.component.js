import React from 'react';
import PropTypes from 'prop-types';
import { Alert as StyledAlert } from './alert.styles';

const AlertComponent = ({ text, severity }) => {
  return (
    <StyledAlert severity={severity}>{text}</StyledAlert>
  );
};

AlertComponent.propTypes = {
  text: PropTypes.string.isRequired,
  severity: PropTypes.string,
  delay: PropTypes.number,
};

AlertComponent.defaultProps = {
  severity: 'success',
  delay: 1000,
};

export const Alert = AlertComponent;