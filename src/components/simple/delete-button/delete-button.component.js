import React, {useState} from 'react';
import {useMutation} from 'react-query';
import {deleteIngredients} from '../../../api/ingredients';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

export const DeleteButton = ({value}) => {
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const {mutate} = useMutation(deleteIngredients, {
    onSuccess: (data) => {
      const message = `success delete ${data.length} ingredients`
      alert(message)
    },
    onError: () => {
      alert("there was an error")
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onSuccess = () => {
    mutate(value)
    handleClose()
  }

  return (
    <div>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
      >Delete</Button>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Waiting..."}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete {`${value.length} ${value.length === 1? 'ingredient': 'ingredients'} ?`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Close
          </Button>
          <Button onClick={onSuccess}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
