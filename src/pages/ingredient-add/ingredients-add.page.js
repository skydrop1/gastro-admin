import React, { useState } from 'react';
import {IngredientForm} from '../../components/complex/forms/ingredient.form';
import {useMutation} from 'react-query';
import {createIngredients} from '../../api/ingredients';
import { Alert } from '../../components/simple/alert/alert.component';

const IngredientsAddPageComponent = (props) => {
  const {} = props;

  const [showSuccess, setSuccess] = useState(false);

  const {mutate} = useMutation(createIngredients, {
    onSuccess: (data) => {
      setSuccess(true);
      setTimeout(() => setSuccess(false), 2000);
    },
    onError: () => {
      alert("there was an error")
    },
  });

  return (
    <div>
      <IngredientForm onSubmit={mutate}/>
      {showSuccess && <Alert text="Item added!" />}
    </div>
  );
};

IngredientsAddPageComponent.propTypes = {};

export const IngredientsAddPage = IngredientsAddPageComponent;