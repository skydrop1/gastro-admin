import styled from 'styled-components';
import {Select} from '../../components/simple/select/select.component';

export const Filter = styled(Select)`
  width: 100px;
`;