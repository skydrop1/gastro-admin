import React, { useContext, useEffect, useMemo, useState } from 'react';
import { useQuery } from 'react-query';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import { getIngredients } from '../../api/ingredients';
import { ingredientsAddRoute } from '../../router/routes';
import { MenuContext } from '../../contexts/menu.context';
import { LinkTable } from '../../components/complex/table/table.component';
import { LinkButton } from '../../components/simple/link-button/link-button.component';
import { DeleteButton } from '../../components/simple/delete-button/delete-button.component';
import { ButtonEdit } from '../../components/simple/edit-button/edit-button.component';
import { groups } from '../../mock/ingredients';
import { allGroup } from '../../mock/common';

import { Filter } from './ingredients.pages.styles';

const IngredientsPageComponent = () => {
  const [group, setGroup] = useState('all');
  const [value, setValue] = useState([]);
  const [selectedRows, setSelectedRows] = useState([]);

  const {data, isSuccess} = useQuery('getIngredients', () => getIngredients(''));
  const {setTitle} = useContext(MenuContext);

  useEffect(() => {
    setTitle('Ingredients');
  }, [setTitle]);

  useEffect(() => {
    if (isSuccess && Array.isArray(data)) {
      setValue(data)
    }
  }, [data, isSuccess])

  const handleChange = (event) => {
    setGroup(event.target.value);
  };

  const items = useMemo(
    () => group === 'all' ? value : value.filter(item => item.group === group),
    [group, value],
  )

  const cells = [
    {field: 'id', headerName: 'id', width: 115},
    {field: 'title', headerName: 'title', width: 115},
    {field: 'group', headerName: 'group', width: 115},
    {
      field: 'createdAt',
      headerName: 'createdAt',
      width: 115,
      renderCell: ({ value }) => new Date(value).toLocaleDateString(),
    },
    {field: 'proteins', headerName: 'proteins', width: 115},
    {field: 'fats', headerName: 'fats', width: 115},
    {field: 'carbohydrates', headerName: 'carbohydrates', width: 115},
    {field: 'calories', headerName: 'calories', width: 115},
    {
      field: 'edit', headerName: 'edit', width: 115, renderCell: ({id}) => <ButtonEdit id={id}/>,
    },
  ];

  return (
    <Box sx={{flexGrow: 1}}>
      <Grid container spacing={2}>
        <Grid item xs={11}>
          <Filter
            title="Groups"
            list={[allGroup, ...groups]}
            value={group}
            onChange={handleChange}
          />
        </Grid>
        {selectedRows.length
          ? (<Grid item xs={1}><DeleteButton value={selectedRows}/></Grid>)
          : (<Grid item xs={1}><LinkButton to={ingredientsAddRoute} label="Create"/></Grid>)
        }
        <Grid item xs={12}>
          <LinkTable
            cells={cells}
            rows={items}
            selectedRows={selectedRows}
            setSelectedRows={setSelectedRows}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export const IngredientsPage = IngredientsPageComponent;