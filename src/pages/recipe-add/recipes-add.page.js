import React, { useEffect, useState } from 'react';
import {IngredientForm} from '../../components/complex/forms/ingredient.form';
import { useMutation, useQuery } from 'react-query';
import { createRecipes } from '../../api/recipes';
import { getIngredients } from '../../api/ingredients';
import { RecipeForm } from '../../components/complex/forms/recipe/recipe.form';

const IngredientsAddPageComponent = (props) => {
  const {} = props;

  const [ingredients, setIngredients] = useState([]);

  const {data, isSuccess} = useQuery('getIngredients', () => getIngredients());

  useEffect(() => {
    if (isSuccess && Array.isArray(data)) {
      setIngredients(data.map(item => ({ ...item, value: item.id })));
    }
  }, [data, isSuccess])

  const {mutate} = useMutation(createRecipes, {
    onSuccess: (data) => {
      const message = `success recipes ${data.title}`
      alert(message)
    },
    onError: () => {
      alert("there was an error")
    },
  });

  return (
    <div>
      <RecipeForm onSubmit={mutate} ingredients={ingredients} />
    </div>
  );
};

IngredientsAddPageComponent.propTypes = {};

export const RecipesAddPage = IngredientsAddPageComponent;