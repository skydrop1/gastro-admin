import React from 'react';
import {useMutation, useQuery} from 'react-query';
import {
  useParams
} from 'react-router-dom';

import {editIngredients, getIngredients} from '../../api/ingredients';

import { IngredientEditForm } from '../../components/complex/forms/ingredient-edit.form';

export const IngredientsEditPageComponent = () => {
  const { id } = useParams();

  const {data, isSuccess} = useQuery('getIngredient', () => getIngredients(id));

  const {mutate} = useMutation(editIngredients, {
    onSuccess: (data) => {
      const message = `edit ingredient ${data.title}`
      alert(message)
    },
    onError: () => {
      alert("there was an error")
    },
  });

  return (
    <div>
      {isSuccess && data ? <IngredientEditForm mutate={mutate} value={data} id={id}/> : null}
    </div>
  );
};

IngredientsEditPageComponent.propTypes = {};

export const IngredientsEditPage = IngredientsEditPageComponent;