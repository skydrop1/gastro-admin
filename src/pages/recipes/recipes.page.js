import React, {useContext, useEffect} from 'react';
import Box from '@material-ui/core/Box';
import {LinkTable} from "../../components/complex/table/table.component";
import { ingredientsAddRoute, recipesAddRoute } from '../../router/routes';
import {LinkButton} from '../../components/simple/link-button/link-button.component';
import {recipesMock} from '../../mock/recipes';
import {MenuContext} from '../../contexts/menu.context';

const RecipesPageComponent = (props) => {
  const {} = props;

  const {setTitle} = useContext(MenuContext);

  useEffect(() => {
    setTitle('Recipes')
  },[setTitle])

  const cells = [
    {field: 'id', headerName: 'id', width: 115},
    {field: 'title', headerName: 'title', width: 115},
    {
      field: 'ingredients',
      headerName: 'ingredients',
      width: 115,
      renderCell: ({ value }) => value.map(({title}) => title).join(', '),
    },
    {
      field: 'actions',
      headerName: 'actions',
      width: 115,
      renderCell: ({ value }) => value.map(({title}) => title).join(', '),
    },
  ];

  return (
    <div>
      <LinkButton
        to={recipesAddRoute}
        label="Create"
      />
      <Box mt={2}>
        <LinkTable
          cells={cells}
          rows={recipesMock}
        />
      </Box>
    </div>
  );
};

export const RecipesPage = RecipesPageComponent;