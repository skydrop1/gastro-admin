import React, { useContext, useEffect } from 'react';
import { MenuContext } from '../../contexts/menu.context';

const DashboardPageComponent = (props) => {
  const {

  } = props;

  const { setTitle } = useContext(MenuContext);

  useEffect(() => {
    setTitle('Dashboard');
  }, [setTitle]);

  return (
    <div>
      1
    </div>
  );
};

DashboardPageComponent.propTypes = {

};

export const DashboardPage = DashboardPageComponent;