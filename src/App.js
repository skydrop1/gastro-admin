import React from 'react';
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query';
import { Router } from './router/router';
import { ThemeProvider } from '@material-ui/core/styles';
import { theme } from './styles/theme';

const queryClient = new QueryClient();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <QueryClientProvider client={queryClient}>
          <Router />
        </QueryClientProvider>
      </div>
    </ThemeProvider>
  );
}

export default App;
