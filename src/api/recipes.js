export const createRecipes = async (data) => {
  await fetch('http://localhost:8000/recipes', {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return data;
};
