export const getIngredients = async (id) => {
  const request = await fetch('http://localhost:8000/ingredients/' + id);
  return await request.json();
};

export const createIngredients = async (data) => {
   await fetch('http://localhost:8000/ingredients', {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return data;
};

export const editIngredients = async (data) => {
  const id = data.id
  delete data.id
  console.log(data, id)
  await fetch('http://localhost:8000/ingredients/' + id, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return data;
};

export const deleteIngredients = async (data) => {
  await data.forEach(item => {
    return fetch('http://localhost:8000/ingredients/'+ item.id, {
      method: 'DELETE',
    });
  })
  return data;
};